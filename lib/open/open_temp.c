/* ported from dietlibcs mkstemp() */

#include "../windoze.h"
#include "../buffer.h"
#include "../open.h"
#include "../str.h"
#include "../uint32.h"

#if WINDOWS_NATIVE
#include <io.h>
#else
#include <limits.h>
#include <unistd.h>
#endif

#include <errno.h>
#include <fcntl.h>

#ifndef O_NOFOLLOW
#define O_NOFOLLOW 0
#endif

#ifndef O_BINARY
#define O_BINARY 0
#endif

static char default_tmpl[PATH_MAX] = "temp-XXXXXX.txt";

int
open_temp(char* tmpl) {
  int i, res;
  unsigned int random;
  char* tmp;

  if(!tmpl) {
    tmpl = default_tmpl;
    tmp = &tmpl[5];
  } else {
    str_copy(default_tmpl, tmpl);
    tmpl = default_tmpl;
  tmp = tmpl + str_len(tmpl) - 6;
  if(tmp < tmpl) goto error;
  }

  for(i = 0; i < 6; ++i) {
    if(tmp[i] != 'X') {
    error:
      errno = EINVAL;
      return -1;
    }

  }
   
  for(;;) {
    random = uint32_random();
    
    for(i = 0; i < 6; ++i) {
      int hexdigit = (random >> (i * 5)) & 0x1f;
      tmp[i] = hexdigit > 9 ? hexdigit + 'A' - 10 : hexdigit + '0';
    }

    unlink(tmpl);
    res = open(tmpl,  O_RDWR | O_CREAT | O_TRUNC | O_BINARY, 0666);
/*
    buffer_putm(buffer_2, "open_temp: '", tmpl, "' = ");
    buffer_putlong(buffer_2, res);
    buffer_putnlflush(buffer_2);
*/
    if(res >= 0 || errno != EEXIST) break;
  }
  return res;
}
