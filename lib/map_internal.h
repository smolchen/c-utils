#include "map.h"
#include "str.h"
#include <stdlib.h>
#include <string.h>

struct map_node_t {
  unsigned hash;
  void* value;
  map_node_t* next;
  /* char key[]; */
  /* char value[]; */
};

map_node_t** map_getref(map_base_t* m, const char* key);

static unsigned
map_hash(const char* str) {
  unsigned hash = 5381;
  while(*str) {
    hash = ((hash << 5) + hash) ^ *str++;
  }
  return hash;
}

static map_node_t*
map_newnode(const char* key, void* value, int vsize) {
  map_node_t* node;
  int ksize = str_len(key) + 1;
  int voffset = ksize + ((sizeof(void*) - ksize) % sizeof(void*));
  node = malloc(sizeof(*node) + voffset + vsize);
  if(!node) return NULL;
  byte_copy(node + 1, ksize, key);
  node->hash = map_hash(key);
  node->value = ((char*)(node + 1)) + voffset;
  byte_copy(node->value, vsize, value);
  return node;
}

static int
map_bucketidx(map_base_t* m, unsigned hash) {
  /* If the implementation is changed to allow a non-power-of-2 bucket count,
   * the line below should be changed to use mod instead of AND */
  return hash & (m->nbuckets - 1);
}

static void
map_addnode(map_base_t* m, map_node_t* node) {
  int n = map_bucketidx(m, node->hash);
  node->next = m->buckets[n];
  m->buckets[n] = node;
}

static int
map_resize(map_base_t* m, int nbuckets) {
  map_node_t *nodes, *node, *next;
  map_node_t** buckets;
  int i;
  /* Chain all nodes together */
  nodes = NULL;
  i = m->nbuckets;
  while(i--) {
    node = (m->buckets)[i];
    while(node) {
      next = node->next;
      node->next = nodes;
      nodes = node;
      node = next;
    }
  }
  /* Reset buckets */
  buckets = realloc(m->buckets, sizeof(*m->buckets) * nbuckets);
  if(buckets != NULL) {
    m->buckets = buckets;
    m->nbuckets = nbuckets;
  }
  if(m->buckets) {
    byte_zero(m->buckets, sizeof(*m->buckets) * m->nbuckets);
    /* Re-add nodes to buckets */
    node = nodes;
    while(node) {
      next = node->next;
      map_addnode(m, node);
      node = next;
    }
  }
  /* Return error code if realloc() failed */
  return (buckets == NULL) ? -1 : 0;
}

